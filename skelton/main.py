﻿# -*- coding: utf-8 -*-
import sys
import operator
import os.path
import glob
import time
import configparser
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt, QThread, pyqtSignal
from PyQt5.Qt import QApplication, QEventLoop, QCursor
from PyQt5.QtWidgets import QFileDialog, QMessageBox
from form import Ui_Dialog

class External(QThread):

    countChanged = pyqtSignal(int)
    memoAdd = pyqtSignal(str)
    finish = pyqtSignal()

    def __init__(self, parent=None):
        super(External, self).__init__(parent)
        self.stopped = False

    def run(self):
        count = 0
        while count < 100:
            if self.stopped:
                self.countChanged.emit(0)
                return
            count +=1
            time.sleep(0.5)
            self.countChanged.emit(count)
            self.memoAdd.emit(str(count))

        self.finished.emit()
        self.finish.emit()

class Form(QtWidgets.QDialog):

    def __init__(self,parent=None):
        super(Form, self).__init__(parent)
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.__running__ = False
        config = configparser.ConfigParser()
        config.read('skelton.ini')
        self.ui.LoadFilePath.setText(config['フォーム']['読み込みファイル'])
        self.ui.LoadFolderPath.setText(config['フォーム']['読み込みフォルダ'])
        self.ui.SaveFilePath.setText(config['フォーム']['保存先ファイル'])
        self.ui.SaveFolderPath.setText(config['フォーム']['保存先フォルダ'])
        
    def onCountChanged(self, value):
        self.ui.progressBar.setValue(value)

    def onMemoAdd(self, value):
        self.ui.LogViewer.append(value)

    def onQuit(self):
        QMessageBox.about(self, "雛形", "終了しました")
        self.EnableControl(True)

    def LoadFileSelect(self):
        path, _ = QFileDialog.getOpenFileName(self)
        if path:
            self.ui.LoadFilePath.setText(path)

    def LoadFolderSelect(self):
        path = QFileDialog.getExistingDirectory(None, "", "")
        if path:
            self.ui.LoadFolderPath.setText(path)

    def SaveFileSelect(self):        
        path, _ = QFileDialog.getSaveFileName(None, "", "")
        if path:
            self.ui.SaveFilePath.setText(path)

    def SaveFolderSelect(self):
        path = QFileDialog.getExistingDirectory(None, "", "")
        if path:
            self.ui.SaveFolderPath.setText(path)

    def EnableControl(self, value: bool):
        self.ui.LoadFilePath.setEnabled(value)
        self.ui.LoadFolderPath.setEnabled(value)
        self.ui.SaveFilePath.setEnabled(value)
        self.ui.SaveFolderPath.setEnabled(value)
        self.ui.LoadFileSelect.setEnabled(value)
        self.ui.LoadFolderSelect.setEnabled(value)
        self.ui.SaveFileSelect.setEnabled(value)
        self.ui.SaveFolderSelect.setEnabled(value)
        self.ui.CloseButton.setEnabled(value)
        if value:
            self.ui.ExecButton.setText("実行");
        else:
            self.ui.ExecButton.setText("キャンセル");

    def reject(self):
        config = configparser.ConfigParser()
        config.read('skelton.ini')
        config['フォーム']['読み込みファイル'] = self.ui.LoadFilePath.text()
        config['フォーム']['読み込みフォルダ'] = self.ui.LoadFolderPath.text()
        config['フォーム']['保存先ファイル'] = self.ui.SaveFilePath.text()
        config['フォーム']['保存先フォルダ'] = self.ui.SaveFolderPath.text()
        with open('skelton.ini', 'w') as configfile:
            config.write(configfile)
        sys.exit(0)

    def accept(self):
        if self.__running__:
            self.calc.stopped = True
            self.__running__ = False
            self.EnableControl(True)
            return

        file = self.ui.LoadFilePath.text()

        if not (os.path.exists(file)):
            QtWidgets.QMessageBox.information(None, "エラー", "指定したファイル「" + file + "」が見つかりませんでした。", QMessageBox.Ok)
            return

        self.EnableControl(False)
        self.__running__ = True
        self.calc = External()

        self.calc.countChanged.connect(self.onCountChanged)
        self.calc.memoAdd.connect(self.onMemoAdd)
        self.calc.finish.connect(self.onQuit)
        self.calc.start()

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    window = Form()
    window.show()
    sys.exit(app.exec_())
